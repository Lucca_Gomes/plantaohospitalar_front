import styles from './button.module.css';
export function Button(props){
    return (
        <input onClick={props.onClick} type='button' value={props.value} className={styles.button} />
    )
}