import styles from './acoes_buttons.module.css';

import PropTypes from "prop-types";
import { useNavigate } from 'react-router-dom'


export function AcoesButtons({ usuarioId, usuarioStatus, loadUsuarios }) {
    const basePath = "http://localhost:4000";

    const navigate = useNavigate();

    function handleAceitarClick() {
        const url = `${basePath}/usuarios/${usuarioId}` 
        const config = { 
            method: "PATCH",
            headers: {
                "Content-Type": "application/json"
            },
            body: {
                "status": true
            }
        }
        fetch(url, config).then(() => {
            loadUsuarios();
        });
    }

    function handleExcluirClick() {
        const url = `${basePath}/usuarios/${usuarioId}` 
        const config = { 
            method: "DELETE",
        }
        fetch(url, config).then(() => {
            loadUsuarios();
        });
    }

    function handleEditarClick() {
        navigate(`/usuarios-admin/editar/${usuarioId}`);
    }

    return <div className={styles.acoesButtonsWrapper}>
        { usuarioStatus ?
            <button className={styles.acoesButton} onClick={handleEditarClick}>
                Editar
            </button>
            :
            <button className={styles.acoesButton} onClick={handleAceitarClick}>
                Aceitar
            </button>
        }

        <button className={styles.acoesButton} onClick={handleExcluirClick}>
            Excluir
        </button>
    </div>
}

AcoesButtons.propTypes = {
    usuarioId: PropTypes.string,
    usuarioStatus: PropTypes.bool,
    loadUsuarios: PropTypes.func
}
