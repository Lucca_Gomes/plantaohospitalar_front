import styles from "./paginacao.module.css";
import { ArrowLeft, ArrowRight, PageButton, RetButton } from './PaginacaoButtons';


export function Paginacao({ paginaIndex, lastPaginaIndex, setPaginaIndex}) {
    var paginacaoConfig = {
        "startRange": 1,
        "endRange": 1,
        "centerRange": 1,
    }
    
    if (window.innerWidth <= 990) {
        paginacaoConfig = {
            "startRange": 1,
            "endRange": 1,
            "centerRange": 1,
        }
    }

    function updatePaginacaoArray() {
        const { startRange, endRange, centerRange } = paginacaoConfig;
        const centerSize = 1 + 2 * centerRange;

        if (lastPaginaIndex <= startRange + centerSize + endRange) {
            return Array.from({ length: lastPaginaIndex }, (_, i) => 1 + i)
        }

        const arrayInicio = Array.from({ length: startRange }, (_, i) => 1 + i);
        const startArrayFinal = 1 + lastPaginaIndex - endRange;
        const arrayFinal = Array.from({ length: endRange }, (_, i) => startArrayFinal + i);

        var arrayCentro = [];

        if (paginaIndex <= startRange || paginaIndex >= startArrayFinal) {  // tipo 1 - index fora
            let arrayPreRet = Array.from({ length: centerRange }, (_, i) => startRange + 1 + i);
            let arrayPosRet = Array.from({ length: centerRange }, (_, i) => lastPaginaIndex - (1 + centerRange) + i)
            
            arrayCentro = arrayPreRet.concat(
                ["..."],
                arrayPosRet
            );
        }

        else if (paginaIndex - (centerRange + 1) <= startRange) {  // tipo 2 - index borda esquerda
            const arrayPreRet = Array.from(
                { length: paginaIndex - startRange + (centerRange - 1) },
                (_, i) => startRange + 1 + i
            );
            const arrayPosRetLength = centerSize - (arrayPreRet.length + 1);
            const arrayPosRet = Array.from(
                { length: arrayPosRetLength },
                (_, i) => startArrayFinal - arrayPosRetLength + i
            )
            
            arrayCentro = arrayPreRet.concat(["..."], arrayPosRet);
        }

        else if (paginaIndex + (centerRange + 1) >= startArrayFinal) {  // tipo 2 - index borda direita
            const arrayPosRetLength = startArrayFinal - paginaIndex + (centerRange - 1);
            const arrayPosRet = Array.from(
                { length: arrayPosRetLength },
                (_, i) => startArrayFinal - arrayPosRetLength + i
            )

            const arrayPreRetLength = centerSize - (arrayPosRetLength + 1);
            const arrayPreRet = Array.from(
                { length: arrayPreRetLength },
                (_, i) => startRange + 1 + i 
            )

            arrayCentro = arrayPreRet.concat(["..."], arrayPosRet);
        }

        else {  // tipo 3 - index centro
            arrayCentro = ["..."].concat(
                Array.from({ length: centerSize - 2 }, (_, i) => paginaIndex - (centerRange - 1) + i),
                ["..."]
            )
        }

        return arrayInicio.concat(arrayCentro, arrayFinal);
    }

    function updatePaginacaoArray2() {
        let paginas = []
        for (var i = 1; i < 1 + paginacaoConfig.startRange; i++) {
            if (i <= lastPaginaIndex){
                paginas.push(i);
            }
        }
    
        if (i + 1 <= paginaIndex - paginacaoConfig.cursorRange) {
            paginas.push("...");
        }
    
        for (var i = paginaIndex - paginacaoConfig.cursorRange; i <= paginaIndex + paginacaoConfig.cursorRange; i++) {
            if (i < 1 || i > lastPaginaIndex) {
                continue;
            }
            
            if (!paginas.includes(i)) {
                paginas.push(i);
            }
        }
    
        if (i + 1 <= 1 + lastPaginaIndex - paginacaoConfig.endRange) {
            paginas.push("...");
        }
    
        for (let i = 1 + lastPaginaIndex - paginacaoConfig.endRange; i <= lastPaginaIndex; i++) {
            if (!paginas.includes(i) && i >= 1) {
                paginas.push(i);
            }
        }
        
        return paginas;
    }

    function handleArrowLeftClick() {
        if (paginaIndex > 1) {
            setPaginaIndex(paginaIndex - 1);
        }
    }

    function handleArrowRightClick() {
        if (paginaIndex < lastPaginaIndex) {
            setPaginaIndex(paginaIndex + 1);
        }
    }

    function handlePageButtonClick(event) {
        const index = parseInt(event.currentTarget.getAttribute('index'));

        setPaginaIndex(index);
    }


    const paginacaoArray = updatePaginacaoArray();
    return <section className={styles.paginacao}>
        <ArrowLeft onClick={handleArrowLeftClick}/>

        {paginacaoArray &&
            paginacaoArray.map((index, i) => {
                return index == "..." ?
                <RetButton key={i}/>
                :
                <PageButton
                    key={i}
                    selected={paginaIndex == index}
                    index={index}
                    onClick={handlePageButtonClick}
                    title={index}
                />
            })
        }

        <ArrowRight  onClick={handleArrowRightClick}/>
    </section>
}
