import PropTypes from "prop-types";
import styles from "./paginacaoButtons.module.css"

export function ArrowLeft({ onClick }) {
    return <img
        className={styles.arrowButton}
        onClick={onClick}
        src="src/components/Paginacao/LeftArrowButton.svg"
        title="Página Anterior"
    />
}

export function ArrowRight({ onClick }) {
    return <img
        className={`${styles.arrowButton} ${styles.arrowButtonRight}`}
        onClick={onClick}
        src="src/components/Paginacao/LeftArrowButton.svg"
        title="Próxima Página"
    />
}

export function PageButton({ selected, index, onClick }) {
    return <div
        className={`${styles.pageButton} ${selected ? styles.pageButtonSelected : styles.pageButtonNotSelected}`}
        index={index}
        onClick={onClick}
    >{index}</div>
}

export function RetButton() {
    return <div className={`${styles.pageButton} ${styles.retButton}`}>...</div>
}


ArrowLeft.propTypes = {
    onClick: PropTypes.func
}

ArrowRight.propTypes = {
    onClick: PropTypes.func
}

PageButton.propTypes = {
    selected: PropTypes.bool,
    index: PropTypes.number,
    onClick: PropTypes.func
}
