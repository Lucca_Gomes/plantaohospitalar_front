import styles from "./form_drop_down_menu.module.css";
import { useState } from 'react';
import PropTypes from "prop-types";

export function FormDropDownMenu({ areasDeAtuacao, handleOnChange }) {
    const [ isOpened, setIsOpened ] = useState(false);

    return <select className={`${styles.select} ${isOpened && styles.selectOpened}`}
        onClick={() => setIsOpened(!isOpened)}
        onBlur={() => setIsOpened(false)}
        onChange={handleOnChange}
    >
        {
            areasDeAtuacao.map((areaDeAtuacao) => {
                return <option
                    className={styles.option}
                    key={areaDeAtuacao.id}
                    value={areaDeAtuacao.id}
                >{areaDeAtuacao.areaDeAtuacao}</option>;
            })
        }
    </select>
}

FormDropDownMenu.propTypes = {
    areasDeAtuacao: PropTypes.array,
    handleOnChange: PropTypes.func
}
