import { useEffect } from 'react';
import styles from './header.module.css';
import { NavLink } from 'react-router-dom';
import { useContext, useState } from 'react';
import { ApplicationContext } from '../../contexts/ApplicationContextProvider';

export function Header() {
    const { token, secretKey } = useContext(ApplicationContext)
    const [username, setUsername] = useState('')
    const [userRole, setUserRole] = useState()
    useEffect(() => {
        const url = `http://localhost:4000/usuarios/sessao/usuario?token=${token}&secretKey=${secretKey}`

        const config = {
            method: "GET"
        }

        fetch(url, config)
            .then(response => response.json())
            .then(data => {
                if (data.erro) {
                    alert(data.erroDescricao)
                } else {
                    setUserRole(data.tipo)
                    setUsername(data.nome)
                }
            })
            .catch(error => {
                console.log(error)
            });
    }, [])

    return (
        <header className={styles.globalAdminHeader}>
            <NavLink to="/" className={styles.headerTitleNavLink}>
                <h1>Sistema Plantão</h1>
            </NavLink>
            <nav>
                {(userRole == 2 || userRole == 3) && <NavLink to="/check-outs" className={styles.headerNavLink}>Check-out</NavLink>}
                {(userRole == 2 || userRole == 3) && <NavLink to="/check-ins" className={styles.headerNavLink}>Check-in</NavLink>}
                {(userRole == 2 || userRole == 3) && <NavLink className={styles.headerNavLink}>Vagas</NavLink>}
                <NavLink className={styles.headerNavLink}>{username}</NavLink>
                <NavLink onClick={() => { localStorage.clear() }} to="/" className={`${styles.headerNavLink} ${styles.headerLogoutNavLink}`}>Sair</NavLink>
            </nav>
        </header>
    )
}