import PropTypes from "prop-types";

import styles from "./card.module.css";

import { ApplicationContext } from '../../contexts/ApplicationContextProvider';
import { useEffect, useState, useContext } from "react";
import { useNavigate } from 'react-router-dom'



export function Card({ plantao, usuario, loadPlantoes, disabled=false }) {
    const { basePath } = useContext(ApplicationContext);
    const navigate = useNavigate();
    const [ vaga, setVaga ] = useState();
    const [ hospital, setHospital ] = useState();
    const [ medico, setMedico ] = useState();
    const [ areaDeAtuacao, setAreaDeAtuacao ] = useState();

    const formatData = { day: "2-digit", month: "2-digit", year: "numeric" };
    const formatHora = { hour: "2-digit", minute: "2-digit" };

    function loadVaga() {
        const url = `${basePath}/vagas/${plantao.vagaId}`;
        const config = { 
            method: "GET", 
        };
        fetch(url, config)
        .then((res) => res.json()
            .then((data) => {
                data.inicio = new Date(data.inicio);
                data.fim = new Date(data.fim);
                setVaga(data);
            })
        );
    }

    function loadHospital() {
        const url = `${basePath}/usuarios/${plantao.hospitalId}`;
        const config = { 
            method: "GET", 
        };
        fetch(url, config)
        .then((res) => res.json()
            .then((data) => {
                setHospital(data);
            })
        );
    }

    function loadMedico() {
        const url = `${basePath}/usuarios/${plantao.medicoId}`;
        const config = { 
            method: "GET", 
        };
        fetch(url, config)
        .then((res) => res.json()
            .then((data) => {
                setMedico(data);
            })
        );
    }

    function loadAreaDeAtuacao() {
        if (!medico) {
            return;
        }

        const url = `${basePath}/areas-de-atuacao/${medico.areaDeAtuacaoId}`;
        const config = { 
            method: "GET", 
        };
        fetch(url, config)
        .then((res) => res.json()
            .then((data) => {
                setAreaDeAtuacao(data);
            })
        );
    }

    function formatCrmCnpj(str) {
        return `${str.substring(0, 10)}-${str.slice(-1)}`;
    }

    function handleCheckinClick() {
        navigate(`./${plantao.id}`);
    }

    function handleExcluirClick() {
        const url = `${basePath}/plantoes/${plantao.id}`;
        const config = { 
            method: "DELETE", 
        };
        fetch(url, config).then(loadPlantoes);
    }

    useEffect(() => {
        loadVaga();

        if (plantao.hospitalId == usuario.id) {
            loadMedico();
        } else {
            loadHospital();
        }

    }, [])

    useEffect(loadAreaDeAtuacao, [ medico ]);

    if (!vaga) {
        return;
    }

    return <div className={styles.cardWrapper}>
        <section className={styles.cardDescricao}>
            <section className={styles.data}>
                <h1>Horário: {vaga.inicio.toLocaleTimeString("pt-BR", formatHora)} - {vaga.fim.toLocaleTimeString("pt-BR", formatHora)}</h1>
                <h1>Data: {vaga.inicio.toLocaleDateString("pt-BR", formatData)}</h1>
            </section>

            {plantao.hospitalId == usuario.id ?
                medico && areaDeAtuacao &&
                <>
                    <h1>Área de Atuação: {areaDeAtuacao.areaDeAtuacao}</h1>
                    <h1>Profissional: {medico.nome}</h1>
                    <h1>CRM {formatCrmCnpj(medico.crm_cnpj)}/BR</h1>
                </>
                :
                hospital &&
                <>
                    <h1>Endereço: {hospital.endereco}</h1>
                </>
            }
            
        </section>

        <section className={styles.buttonsWrapper}>
            <button onClick={handleCheckinClick} disabled={disabled}>Check-in</button>
            <button onClick={handleExcluirClick} disabled={disabled}>Excluir</button>
        </section>
    </div>
}


Card.propTypes = {
    plantao: PropTypes.object,
    usuario: PropTypes.object,
    loadPlantoes: PropTypes.func,
    disabled: PropTypes.bool
}