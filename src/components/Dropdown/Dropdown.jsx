import React, { useEffect, useState } from 'react';
import styles from './dropdown.module.css';

export function Dropdown({ selectedFieldIndex, setSelectedFieldIndex, ...props }) {
    const [isOpen, setIsOpen] = useState(false);
    const [selectedField, setSelectedField] = useState(null);
    const [fields, setFields] = useState([]);
    let urlApi = "http://localhost:4000/areas-de-atuacao"
    let fetchConfig = {
        "method": "GET"
    }
    useEffect(() => {
        fetch(urlApi, fetchConfig)
            .then((resposta) => {
                resposta.json()
                    .then((resposta) => {
                        setFields(resposta.map(({ areaDeAtuacao }) => areaDeAtuacao))
                    })
                    .catch((error) => {
                        console.log(error);
                    })
            })
            .catch((error) => {
                console.log(error);
            });
    }, []);
    const toggleDropdown = () => {
        setIsOpen(!isOpen);
    };

    const handleFieldClick = (field) => {
        setSelectedField(field);
        setSelectedFieldIndex(fields.indexOf(field));
        setIsOpen(false);
    };

    return (
        <div className={styles.dropdown}>
            <label htmlFor='selected-field'>{props.label}</label>
            <div className={styles.dropdownContainer} onClick={toggleDropdown}>
                <div id='selected-field' className={styles.dropdownToggle}>
                    {selectedField || props.placeholder}
                    <img src="/src/assets/selector.svg" />
                </div>
                {isOpen && (
                    <ul className={styles.dropdownList}>
                        {fields.map((field, index) => (
                            <li className={styles.dropdownItem} key={index} onClick={() => handleFieldClick(field)}>
                                {field}
                            </li>
                        ))}
                    </ul>
                )}
            </div>
        </div>
    );
}
