import styles from './search.module.css';
export function Search(props){
    return(
        <div className={styles.searchInputContainer}>
            <label htmlFor="search">{props.label}</label>
            <div id="search"className={styles.searchField}>
                <input type="text" />
                <img src="src/assets/search.svg" alt="" />
            </div>
        </div>
    )
}