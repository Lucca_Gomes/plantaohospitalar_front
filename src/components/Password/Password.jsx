import styles from './password.module.css';
import React from 'react';

export const Password = React.forwardRef((props, ref) => {
    function togglePassword() {
        document.getElementById("password").type = document.getElementById("password").type === "password" ? "text" : "password";
    }
    return (
        <div className={styles.passwordInputContainer}>
            <label htmlFor="password">Senha</label>
            <div className={styles.passwordField}>
                <input ref={ref} id="password" type="password" />
                <img src="/src/assets/eye.svg" alt="" onClick={togglePassword} />
            </div>
        </div>
    );
});