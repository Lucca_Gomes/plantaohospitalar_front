import "./global.css"
import { BrowserRouter, Routes, Route } from "react-router-dom"

import { Login } from "./pages/Login/Login";
import { UsuariosAdmin } from "./pages/UsuariosAdmin/UsuariosAdmin";
import { UsuariosAdminEditar } from "./pages/UsuariosAdminEditar/UsuariosAdminEditar";
import { Account } from "./pages/Contas/Account.jsx"
import { AccDoctor } from "./pages/Médico/AccDoctor"
import { CheckIns } from "./pages/CheckIns/CheckIns";
import { CheckIn } from "./pages/CheckIn/CheckIn";

import { ApplicationContextProvider } from "./contexts/ApplicationContextProvider"
import { BaseLayout } from "./layouts/BaseLayout"

import { AccHospital } from "./pages/Hospital/AccHospital";
import { EditarVagas } from "./pages/EditarVagas/editarVagas";
import { CadastrarVaga } from "./pages/CadastrarVaga/CadastrarVaga";

import { CheckOuts } from "./pages/CheckOuts/CheckOuts";
import { CheckOut } from "./pages/CheckOut/CheckOut";

function App() {
  return (
    <>
      <BrowserRouter>
        <ApplicationContextProvider>
          <Routes>
            <Route path='/' element={<Login />} />
            <Route path="/usuarios-admin" element={<BaseLayout />}>
              <Route path="/usuarios-admin" element={<UsuariosAdmin />} />
              <Route path="/usuarios-admin/editar/:id" element={<UsuariosAdminEditar />} />
            </Route>
            <Route path='/contas' element={<BaseLayout />}>
              <Route path='/contas' element={<Account />}/>
              <Route path='/contas/cadastro-medico' element={<AccDoctor />} />
              <Route path='/contas/cadastro-hospital' element={<AccHospital />}/>
            </Route>

            <Route path="/vagas" element={<BaseLayout />}>
              <Route path="/vagas/editar/:id" element={<EditarVagas />}/>
            </Route>

            <Route path='check-ins' element={<BaseLayout />}>
              <Route path='/check-ins' element={<CheckIns />}/>
              <Route path='/check-ins/:id' element={<CheckIn />}/>
            </Route>

            <Route path='check-outs' element={<BaseLayout />}>
              <Route path='/check-outs' element={<CheckOuts />}/>
              <Route path='/check-outs/:id' element={<CheckOut />}/>
            </Route>

            <Route path='/hospital-admin' element={<BaseLayout />}>
              <Route path='/hospital-admin/cadastro-vaga' element={<CadastrarVaga />} />
            </Route>

          </Routes>
        </ApplicationContextProvider>
      </BrowserRouter>
    </>
  )
}
export default App
