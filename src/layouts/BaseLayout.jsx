import { Outlet } from "react-router-dom";
import { Header } from "../components/Header/Header";
export function BaseLayout() {
    return (
        <>
            <Header />
            <Outlet />
        </>
    )
}