import { createContext, useState, useEffect } from "react";
import PropTypes from "prop-types";

export const ApplicationContext = createContext();
export function ApplicationContextProvider({ children }) {
    const [token, setToken] = useState("eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6IjAwMDIiLCJpYXQiOjE2OTUyMjY1MjYsImV4cCI6MTY5NTIzMDEyNn0.2FRlmA9fQX8xDYq7vTm5ePWUGprNCPzEuPT1EtbmQYI");
    const [secretKey, setSecretKey] = useState("71e6b371-b0e8-4cb7-a298-366970f15691");

    const [ basePath, setBasePath ] = useState("http://localhost:4000");

    async function getUser() {
        if (!token || !secretKey) {
            return;
        }
        
        const url = `${basePath}/usuarios/sessao/usuario?token=${token}&secretKey=${secretKey}`;
        const config = { 
            method: "GET", 
        };
        const res = await fetch(url, config)
    
        const data = await res.json();

        return data;
    }

    async function getUserTipo() {
        const usuario = await getUser();
        const url = `${basePath}/tipos-de-usuarios/${usuario.tipo}`;
        const config = { 
            method: "GET", 
        };
        const res = await fetch(url, config);
        const data = await res.json();

        return data.tipo;
    }

    return (
        <ApplicationContext.Provider 
            value={{
                token, setToken,
                secretKey, setSecretKey,
                basePath, setBasePath,
                getUser,
                getUserTipo
            }}
        >
            {children}
        </ApplicationContext.Provider>
    )
}
ApplicationContextProvider.propTypes = {
    children: PropTypes.node
}
