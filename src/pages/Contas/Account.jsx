import styles from './Account.module.css'
import { NavLink } from 'react-router-dom'

export function Account() {
    return(
        <div className={styles.accountMain}>
            <div className={styles.accountContainer}>
                <h2>Escolha o seu tipo de conta</h2>
                <NavLink to="cadastro-hospital" className={styles.accountNavLink} >
                        Conta Hospital
                </NavLink>
                <NavLink to="cadastro-medico" className={styles.accountNavLink} /*to=''*/>
                        Conta Médico
                </NavLink>
            </div>
        </div>
    )
}