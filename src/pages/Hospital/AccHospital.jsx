import styles from './AccHospital.module.css'
import { Password } from '../../components/Password/Password'
import { useRef } from 'react';
import { Button } from '../../components/Button/Button';
import { NavLink } from 'react-router-dom';

export function AccHospital(){

    const passwordRef = useRef(null);
    let urlApi = "http://localhost:4000/usuarios/sessao/login";
    
    function handleNewUser(event) {
        event.preventDefault();

        const nome = document.getElementById("nome").value;
        const cnpj = document.getElementById("cnpj").value;
        const endereco = document.getElementById("endereco").value;
        const email = document.getElementById("email").value;
        const senha = passwordRef.current.value;
        console.log(nome, cnpj, endereco, email, senha);
        
        let fetchBody = {
            "nome": nome,
            "crm_cnpj": cnpj,
            "endereco": endereco,
            "email": email,
            "senha": senha,
            "tipo": 2
        }
        fetchConfig = {
            "method": "POST",
            "body": JSON.stringify(fetchBody),
            "headers": { "Content-Type": "application/json" }
        }

        fetch(urlApi, fetchConfig)
            .then((resposta) => {
                resposta.json()
                    .then((resposta) => {
                        console.log(resposta);
                    })
                    .catch((error) => {
                        console.log(error);
                    });
            })
            .catch((error) => {
                console.log(error);
            });
    }

    return(
        <>
        <div className={styles.mainAccHospital}>
            <div className={styles.titleContainer}>
                <h2>Cadastro Hospital</h2>
            </div>
            <div className={styles.formContainer}>
                <div className={styles.registerForm}>
                    <label htmlFor="nome">Nome</label>
                    <input className={styles.doctorAccountInput} name="nome" id="nome" type="text" />
                    <label htmlFor="cnpj">CNPJ</label>
                    <input className={styles.doctorAccountInput} name="cnpj" id="cnpj" type="text" />
                    <label htmlFor="endereco">Endereço</label>
                    <input className={styles.doctorAccountInput} name="endereco" id="endereco" type="text" />
                    <label htmlFor="email">Email</label>
                    <input className={styles.doctorAccountInput} name="email" id="email" type="text" />
                    <Password ref={passwordRef}/>
                    <div className={styles.buttonsWrapper}>
                        <Button value='Cadastrar' onClick={handleNewUser} />
                        <NavLink to="/"className={styles.cancelButton}>Cancelar</NavLink>

                    </div>
                </div>
            </div>
        </div>
        </>

    )
}