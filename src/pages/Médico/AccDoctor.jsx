import { Dropdown } from '../../components/Dropdown/Dropdown';
import { Button } from '../../components/Button/Button';
import { Password } from '../../components/Password/Password';
import styles from '../Médico/AccDoctor.module.css'
import { NavLink } from 'react-router-dom';
import { useState, useRef } from 'react';

export function AccDoctor() {
    const passwordRef = useRef(null);
    const [parentSelectedFieldIndex, setParentSelectedFieldIndex] = useState(null);

    function handleNewUser(event) {
        event.preventDefault();

        urlApi = "http://localhost:4000/usuarios/sessao/login";

        const nome = document.getElementById("nome").value;
        const cpf = document.getElementById("cpf").value;
        const crm = document.getElementById("crm").value;
        const areaId = areasList[parentSelectedFieldIndex].id
        const email = document.getElementById("email").value;
        const senha = passwordRef.current.value;

        let fetchBody = {
            "nome": nome,
            "cpf": cpf,
            "crm_cnpj": crm,
            "areaDeAtuacaoId": areaId,
            "email": email,
            "senha": senha,
            "tipo": 3
        }
        fetchConfig = {
            "method": "POST",
            "body": JSON.stringify(fetchBody),
            "headers": { "Content-Type": "application/json" }
        }

        fetch(urlApi, fetchConfig)
            .then((resposta) => {
                resposta.json()
                    .then((resposta) => {
                        
                    })
                    .catch((error) => {
                        console.log(error);
                    });
            })
            .catch((error) => {
                console.log(error);
            });
    }


    return (
        <div className={styles.mainAccDoctor}>
            <div className={styles.titleContainer}><h2>Cadastro Médico</h2></div>
            <div className={styles.formContainer}>
                <form className={styles.registerForm}>
                    <label htmlFor="nome">Nome</label>
                    <input className={styles.doctorAccountInput} name="nome" id="nome" type="text" />
                    <label htmlFor="cpf">CPF</label>
                    <input className={styles.doctorAccountInput} name="cpf" id="cpf" type="text" />
                    <label htmlFor="crm">CRM</label>
                    <input className={styles.doctorAccountInput} name="crm" id="crm" type="text" />
                    <Dropdown selectedFieldIndex={parentSelectedFieldIndex} setSelectedFieldIndex={setParentSelectedFieldIndex} label='Área de atuação' placeholder='Escolha uma área' />
                    <label htmlFor="email">Email</label>
                    <input className={styles.doctorAccountInput} name='email' id='email' type="email" />
                    <Password ref={passwordRef} />
                    <div className={styles.buttonsWrapper}>
                        <Button value='Cadastrar' onClick={handleNewUser} />
                        <NavLink to="/" className={styles.cancelButton}>Cancelar</NavLink>
                    </div>
                </form>
            </div>
        </div>
    )
}