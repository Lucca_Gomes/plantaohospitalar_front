import styles from "./login.module.css"
import { NavLink, useNavigate } from "react-router-dom"
import { ApplicationContext } from "../../contexts/ApplicationContextProvider"
import { React, useContext, useRef } from "react"
import { Button } from "../../components/Button/Button"
import { Password } from "../../components/Password/Password"

export function Login() {
    const passwordRef = useRef(null);
    const {  setToken, setSecretKey,  } = useContext(ApplicationContext)
    const navigate = useNavigate()

    
    function handleLogin(event) {
        event.preventDefault();
    
        const email = document.querySelector('#email').value
        const password = passwordRef.current.value
    
        let url = `http://localhost:4000/usuarios/sessao/login?email=${email}&senha=${password}`
        let config = {
            method: "GET"
        }
    
        fetch(url, config)
            .then(response => response.json())
            .then(data => {
                if (data.erro) {
                    alert(data.erroDescricao)
                } else {
                    setToken(data.token);
                    setSecretKey(data.secretKey);
                    redirect(data.token, data.secretKey); // Passando os valores atualizados como parâmetros
                }
            })
            .catch(error => {
                console.log(error)
            });
    }
    
    function redirect(token, secretKey) { // Recebendo os valores atualizados como parâmetros
        let url = `http://localhost:4000/usuarios/sessao/usuario?token=${token}&secretKey=${secretKey}`
        let config = {
            method: "GET"
        }
        fetch(url, config)
            .then(response => response.json())
            .then(data => {
                if (data.erro) {
                    alert(data.erroDescricao)
                } else {
                    if (data.tipo == 2) {
                        navigate('/hospital-admin/cadastro-vaga')
                    } else if (data.tipo == 1) {
                        navigate('/usuarios-admin')
                    }
                }
            })
            .catch(error => {
                console.log(error)
            });
    }



    return (
        <div className={styles.loginContainer}>
            <div className={styles.loginForm}>
                <form>
                    <label htmlFor="email">E-mail</label>
                    <input type="email" id="email" className={styles.loginInput} />
                    <Password ref={passwordRef} />
                    <Button value="Login" onClick={handleLogin} />
                </form>
                <hr />
                <NavLink to="/contas" className={styles.loginNavLink}>Crie sua conta</NavLink>
            </div>
        </div>
    )
}
