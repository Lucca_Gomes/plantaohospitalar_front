import styles from "./CadastrarVaga.module.css";
import { Button } from "../../components/Button/Button";
import { NavLink } from "react-router-dom";
import { useState, useEffect } from "react";
import { Dropdown } from "../../components/Dropdown/Dropdown";
import { useContext } from "react";
import { ApplicationContext } from "../../contexts/ApplicationContextProvider";


export function CadastrarVaga() {
    const [parentSelectedFieldIndex, setParentSelectedFieldIndex] = useState(null);
    const { token, secretKey } = useContext(ApplicationContext)
    const [hospitalId, setHospitalId] = useState()
    function validarValor() {
        var valorInput = document.getElementById("valor");

        if (valorInput.value < 0) { valorInput.value = 0; }
    }
    useEffect(() => {
        const url = `http://localhost:4000/usuarios/sessao/usuario?token=${token}&secretKey=${secretKey}`

        const config = {
            method: "GET"
        }

        fetch(url, config)
            .then(response => response.json())
            .then(data => {
                if (data.erro) {
                    alert(data.erroDescricao)
                } else {
                    setHospitalId(data.id)
                }
            })
            .catch(error => {
                console.log(error)
            });
    }, [hospitalId])


    function handleNewVaga() {
        urlApi = `http://localhost:4000/vagas?token=${token}&secretKey=${secretKey}`;

        const horarioInicio = document.getElementById("start-time").value;
        const horarioFinal = document.getElementById("end-time").value;
        const data = document.getElementById("data").value;
        const valor = document.getElementById("valor").value;
        const areaId = areasList[parentSelectedFieldIndex].id


        let fetchBody = {
            "horarioDeInicio": horarioInicio,
            "horarioDeTermino": horarioFinal,
            "data": data,
            "valor": valor,
            "hospitalId": hospitalId,
            "areaDeAtuacaoId": areaId,
        }

        fetchConfig = {
            "method": "POST",
            "body": JSON.stringify(fetchBody),
            "headers": { "Content-Type": "application/json" }
        }

        fetch(urlApi, fetchConfig)
            .then((resposta) => {
                resposta.json()
                    .then((resposta) => {

                    })
                    .catch((error) => {
                        console.log(error);
                    });
            })
            .catch((error) => {
                console.log(error);
            });
    }

    return (
        <div className={styles.mainAccDoctor}>
            <div className={styles.titleContainer}><h2>Cadastrar Vaga</h2></div>
            <div className={styles.formContainer}>
                <form className={styles.registerForm}>
                    <div className={styles.cadastrarVagaTimes}>
                        <div className={styles.cadastrarVagaTime}>
                            <label htmlFor="start-time">Horário de Início</label>
                            <input className={styles.doctorAccountInput} name="start-time" id="start-time" type="time" />
                        </div>
                        <div className={styles.cadastrarVagaTime}>
                            <label htmlFor="cpf">Horário de Término</label>
                            <input className={styles.doctorAccountInput} name="end-time" id="end-time" type="time" />
                        </div>
                    </div>
                    <label htmlFor="data">Data</label>
                    <input className={styles.doctorAccountInput} placeholder="00/00/0000" name="data" id="data" type="date" />
                    <label htmlFor="valor">Valor</label>
                    <input className={styles.doctorAccountInput} placeholder="R$ 0000,00" name="valor" step="0.01" id="valor" type="number" onInput={validarValor} />
                    <Dropdown selectedFieldIndex={parentSelectedFieldIndex} setSelectedFieldIndex={setParentSelectedFieldIndex} label='Área de atuação' placeholder='Escolha uma área' />
                    <div className={styles.buttonsWrapper}>
                        <Button value='Cadastrar' onClick={handleNewVaga} />
                        <NavLink to="/" className={styles.cancelButton}>Cancelar</NavLink>
                    </div>
                </form>
            </div>
        </div>
    )
}