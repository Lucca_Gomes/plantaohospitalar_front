import styles from './usuarios_admin_editar.module.css';

import { useState, useEffect, useContext } from 'react';
import { useParams, useNavigate } from 'react-router-dom';

import { FormDropDownMenu } from "../../components/UsuariosAdminEditar/FormDropDownMenu";

import { ApplicationContext } from '../../contexts/ApplicationContextProvider';

export function UsuariosAdminEditar() {
    const { basePath, token, secretKey } = useContext(ApplicationContext);

    const { id } = useParams();
    const navigate = useNavigate();

    const [ areasDeAtuacao, setAreasDeAtuacao ] = useState();

    const [ usuario, setUsuario ] = useState();
    const [ usuarioTipo, setUsuarioTipo ] = useState();
    const [ formData, setFormData ] = useState({});

    function loadUsuario() {
        const url = `${basePath}/usuarios/${id}?adminToken=${token}&adminSecretKey=${secretKey}` 
        const config = { 
            method: "GET", 
        }
        fetch(url, config) 
        .then(response => { response.json() 
            .then(data => {
                setUsuario(data);
                console.log(data);

                setFormData({
                    "nome": data.nome,
                    "crm_cnpj": data.crm_cnpj,
                    "cpf": data.cpf,
                    "areaDeAtuacaoId": data.areaDeAtuacaoId,
                    "endereco": data.endereco,
                    "email": data.email,
                    "senha": ""
                });
            })
        })
    }

    function loadUsuarioTipo() {
        if (!usuario) {
            return;
        }

        const url = `${basePath}/tipos-de-usuarios/${usuario.tipo}` 
        const config = { 
            method: "GET", 
        }
        fetch(url, config) 
        .then(response => { response.json() 
            .then(data => {
                setUsuarioTipo(data.tipo);
                console.log(data.tipo)
            })
        })
    }

    function loadAreasDeAtuacao() {
        const url = `${basePath}/areas-de-atuacao` 
        const config = { 
            method: "GET", 
        }
        fetch(url, config) 
        .then(response => { response.json() 
            .then(data => {
                setAreasDeAtuacao(data);
            })
        })
    }

    function handleSubmit(event) {
        event.preventDefault();
        const url = `${basePath}/usuarios/${id}?adminToken=${token}&adminSecretKey=${secretKey}` 
        
        let requestBody = { ...formData };

        for (const [key, value] of Object.entries(requestBody)) {
            if (!value || !/\S/.test(value)) {
                delete requestBody[key];
            }
        }

        console.log(requestBody);

        const config = { 
            method: "PATCH",
            headers: {
                "Content-Type": "application/json"
            },
            body: JSON.stringify(requestBody)
        }
        fetch(url, config).then(() => {
            navigate("/usuarios-admin");
        })
    }

    function handleFormOnChange(propName, event) {
        let updatedFormData = { ...formData };

        updatedFormData[propName] = /\S/.test(event.target.value) ? event.target.value : "";
        setFormData(updatedFormData);
    }

    function handleCancelarClick() {
        navigate("/usuarios-admin");
    }

    useEffect(() => {
        loadUsuario()
        loadAreasDeAtuacao()
    }, []);

    useEffect(loadUsuarioTipo, [usuario, basePath]);

    if (!usuario) {
        return;
    }

    return <div className={styles.usuariosAdminEditarWrapper}>
        <h1 className={styles.paginaTitulo}>Editar {usuarioTipo == "hopital" ? "Hospital" : "Médico"}</h1>

        <form className={styles.form} onSubmit={handleSubmit}>
            <section name="main" className={styles.formMainSection}>
                <label>Nome</label>
                <input name="nome" type="text" value={formData.nome} onChange={(event) => handleFormOnChange("nome", event)}></input>

                {{
                    "hospital": <>
                        <label>CNPJ</label>
                        <input name="crm_cnpj" type="text" value={formData.crm_cnpj} onChange={(event) => handleFormOnChange("crm_cnpj", event)}></input>
                        <label>Endereço</label>
                        <input name="endereco" type="text" value={formData.endereco} onChange={(event) => handleFormOnChange("endereco", event)}></input>
                    </>,
                    "medico": <>
                        <label>CPF</label>
                        <input name="cpf" type="text" value={formData.cpf} onChange={(event) => handleFormOnChange("cpf", event)}></input>
                        <label>CRM</label>
                        <input name="crm_cnpj" type="text" value={formData.crm_cnpj} onChange={(event) => handleFormOnChange("crm_cnpj", event)}></input>

                        <label>Area de Atuação</label>
                        <FormDropDownMenu areasDeAtuacao={areasDeAtuacao} defaultValue={usuario.areaDeAtuacaoId} handleOnChange={(event) => handleFormOnChange("areaDeAtuacaoId", event)}></FormDropDownMenu>
                    </>
                }[usuarioTipo]}

                <label>Email</label>
                <input name="email" type="text" value={formData.email} onChange={(event) => handleFormOnChange("email", event)}></input>

                <label>Senha</label>
                <input name="senha" type="text" value={formData.senha} placeholder="Nova Senha" onChange={(event) => handleFormOnChange("senha", event)}></input>
            </section>

            <section className={styles.formButtonsSection}>
                <button className={styles.cadastrarButton} type="submit">Cadastrar</button>
                <button className={styles.cancelarButton} type="button" onClick={handleCancelarClick}>Cancelar</button>
            </section>
        </form>

    </div>
}
