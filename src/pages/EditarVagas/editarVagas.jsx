import styles from './editarVagas.module.css'
import { useState,useEffect } from 'react';
import { useParams} from 'react-router-dom';
import { Button } from '../../components/Button/Button';
import { NavLink, useNavigate } from 'react-router-dom';

import { FormDropDownMenu } from "../../components/EditarVagas/FormDropDownMenu";

export function EditarVagas(){
    const { id } = useParams();
    const [ formData, setFormData ] = useState({
        inicio: "",
        fim: "",
        valor: "",
        areaDeAtuacaoId: ""
    });

    const [ vaga, setVaga ] = useState();
    const [ areasDeAtuacao, setAreasDeAtuacao ] = useState();

    const basePath = "http://localhost:4000";

    const timeFormat = { hour: "2-digit", minute: "2-digit" }

    const navigate = useNavigate();

    function loadAreasDeAtuacao() {
        const url = `${basePath}/areas-de-atuacao` 
        const config = { 
            method: "GET", 
        }
        fetch(url, config) 
        .then(response => { response.json() 
            .then(data => {
                setAreasDeAtuacao(data);
            })
        })
    }

    function loadVaga() {
        const url = `${basePath}/vagas/${id}` 
        const config = { 
            method: "GET", 
        }
        fetch(url, config) 
        .then(response => { response.json() 
            .then(data => {
                setVaga(data);

                setFormData({
                    inicio: new Date(data.inicio).toLocaleTimeString("pt-BR", timeFormat),
                    fim: new Date(data.fim).toLocaleTimeString("pt-BR", timeFormat),
                    valor: data.valor,
                    areaDeAtuacaoId: data.areaDeAtuacaoId
                });
            })
        })
    }

    function handleFormOnChange(propName, event) {
        let updatedFormData = { ...formData };

        updatedFormData[propName] = /\S/.test(event.target.value) ? event.target.value : "";
        setFormData(updatedFormData);
    }

    function handleEditUser(event) { /* estava tentando fazer o patch, mas acho que fiz tudo errado, estou adicionando de novo pq a pag parou de rodar */
        event.preventDefault();

        const url = `${basePath}/vagas/${id}`

        const body = { ...formData };

        body.inicio = new Date(vaga.inicio.split("T")[0] + "T" + body.inicio);
        body.fim = new Date(vaga.fim.split("T")[0] + "T" + body.fim);

        const fetchConfig = {
            "method": "PATCH",
            "body": JSON.stringify(body),
            "headers": { "Content-Type": "application/json" }
        }

        fetch(url, fetchConfig)
            .then((resposta) => {
                resposta.json()
                    .then(() => {
                        navigate("../")
                    })
                    .catch((error) => {
                        console.log(error);
                    });
            })
            .catch((error) => {
                console.log(error);
            });
    }

    useEffect(() => {
        loadVaga();
        loadAreasDeAtuacao();
    }, []);

    if (!areasDeAtuacao) {
        return
    }

    return(
        <div className={styles.mainEdit}>
            <div className={styles.titleContainer}><h2>Editar Vaga</h2></div>
            <div className={styles.formContainer}>
                <form className={styles.editForm}>
                    <div className={styles.horariosWrapper}>
                        <div className={styles.inicio}>
                            <label htmlFor="nome">Horário de início</label>
                            <input className={styles.inputTime} name="nome" id="nome" type="text" value={formData.inicio} onChange={(event) => handleFormOnChange("inicio", event)} />
                        </div>
                        <div className={styles.termino}>
                            <label htmlFor="cpf">Horário de término</label>
                            <input className={styles.inputTime} name="cpf" id="cpf" type="text" value={formData.fim} onChange={(event) => handleFormOnChange("fim", event)}/>
                        </div>
                    </div>
                    <div className={styles.containerValor}>
                        <label htmlFor="valor">Valor</label>
                        <input className={styles.editInput} name="crm" id="crm" type="text" placeholder='$0000,00' value={formData.valor} onChange={(event) => handleFormOnChange("valor", event)} />
                    </div>
                    <div className={styles.dropdown}>

                        <FormDropDownMenu areasDeAtuacao={areasDeAtuacao} handleOnChange={(event) => handleFormOnChange("areaDeAtuacaoId", event)} />
                    </div>
                    <div className={styles.buttonsWrapper}>
                        <Button value='Cadastrar' onClick={handleEditUser} />
                        <NavLink to="/" className={styles.cancelButton}>Cancelar</NavLink>
                    </div>
                </form>
            </div>
        </div>
    )
}