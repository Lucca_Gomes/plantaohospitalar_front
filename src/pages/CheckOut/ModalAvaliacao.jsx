import PropTypes from "prop-types";

import styles from "./modalAvaliacao.module.css";
import { useState } from "react";

export function ModalAvaliacao({ aberto, handleSubmit }) {
    const [ formData, setFormData ] = useState({nota: "0.0", avaliacao: ""});

    function handleFormOnChange(propName, event) {
        let updatedFormData = { ...formData };

        updatedFormData[propName] = /\S/.test(event.target.value) ? event.target.value : "";
        setFormData(updatedFormData);
    }

    return aberto &&
    <form className={styles.modalAvaliacao} onSubmit={(event) => handleSubmit(event, formData)}>
        <section>
            <label>Nota</label>
            <input type="text" placeholder="0.0" value={formData.nota} onChange={(event) => handleFormOnChange("nota", event)}/>

            <label>Avaliação</label>
            <textarea rows="10" cols="40" value={formData.avaliacao} onChange={(event) => handleFormOnChange("avaliacao", event)}/>
        </section>
        <button type="submit">Enviar</button>
    </form>
}


ModalAvaliacao.propTypes = {
    aberto: PropTypes.bool,
    handleSubmit: PropTypes.func
}
