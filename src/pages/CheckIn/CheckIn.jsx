import { useState, useEffect, useContext } from 'react';

import styles from "./check-in.module.css";

import { Card } from "../../components/CheckIns/Card";

import { ApplicationContext } from '../../contexts/ApplicationContextProvider';
import { useParams, useNavigate } from 'react-router-dom';

export function CheckIn() {
    const { basePath, getUser, getUserTipo } = useContext(ApplicationContext);
    const { id: plantaoId } = useParams();
    const navigate = useNavigate();

    const [ usuario, setUsuario ] = useState();
    const [ usuarioTipo, setUsuarioTipo ] = useState();
    const [ plantao, setPlantao ] = useState();
    const [ vaga, setVaga ] = useState();
    const [ medico, setMedico ] = useState();
    const [ areaDeAtuacao, setAreaDeAtuacao ] = useState();
    const [ media, setMedia ] = useState();

    const [ codigoAutorizacao, setCodigoAutorizacao ] = useState("");

    const formatData = { day: "2-digit", month: "2-digit", year: "numeric" };
    const formatHora = { hour: "2-digit", minute: "2-digit" };

    function loadPlantao() {
        const url = `${basePath}/plantoes/${plantaoId}`;
        const config = { 
            method: "GET", 
        };
        fetch(url, config) 
        .then(response => { response.json() 
            .then(data => {
                setPlantao(data);
            });
        });
    }

    function loadVaga() {
        if (!plantao) {
            return;
        }

        const url = `${basePath}/vagas/${plantao.vagaId}`;
        const config = { 
            method: "GET", 
        };
        fetch(url, config)
        .then((res) => res.json()
            .then((data) => {
                data.inicio = new Date(data.inicio);
                data.fim = new Date(data.fim);
                setVaga(data);
            })
        );
    }

    function loadMedico() {
        if (!plantao) {
            return;
        }
        
        const url = `${basePath}/usuarios/${plantao.medicoId}`;
        const config = { 
            method: "GET", 
        };
        fetch(url, config)
        .then((res) => res.json()
            .then((data) => {
                setMedico(data);
            })
        );
    }

    function loadAreaDeAtuacao() {
        if (!medico) {
            return;
        }

        const url = `${basePath}/areas-de-atuacao/${medico.areaDeAtuacaoId}`;
        const config = { 
            method: "GET", 
        };
        fetch(url, config)
        .then((res) => res.json()
            .then((data) => {
                setAreaDeAtuacao(data);
            })
        );
    }

    function loadMedia() {
        if (!medico) {
            return;
        }

        const url = `${basePath}/avaliacoes/media/${medico.id}`;
        const config = { 
            method: "GET", 
        };
        fetch(url, config)
        .then((res) => res.json()
            .then((data) => {
                setMedia(data);
            })
        );
    }

    function formatCrmCnpj(str) {
        return `${str.substring(0, 10)}-${str.slice(-1)}`;
    }

    function handleAutorizacaoInputChange(event) {
        setCodigoAutorizacao(event.target.value);
    }

    function handleFormSubmit(event) {
        event.preventDefault();

        if (codigoAutorizacao != plantao.autorizacaoCheckIn) {
            alert("Código de Autorização Inválido")
            return;
        }

        const url = `${basePath}/plantoes/${plantaoId}`;
        const config = { 
            method: "PATCH",
            headers: {
                "Content-Type": "application/json"
            },
            body: JSON.stringify({
                "disponivelPara": "check-out"
            })
        };
        fetch(url, config).then(() => navigate("../check-ins"));
    }

    useEffect(() => {
        getUser().then((usuario) => setUsuario(usuario));
        getUserTipo().then((usuarioTipo) => setUsuarioTipo(usuarioTipo));
        loadPlantao();
    }, [])

    useEffect(() => {
        if (usuario && usuario.id == plantao.hospitalId) {
            loadVaga();
            loadMedico();
        }
    }, [ plantao ])

    useEffect(() => {
        loadAreaDeAtuacao();
        loadMedia();
    }, [ medico ]);

    if (!plantao) {
        return;
    }

    return <div className={styles.checkInWrapperWrapper}>
        <div className={`${styles.checkInWrapper} ${usuarioTipo == "medico" ? styles.checkInWrapperDarker : ""}`}>

        <h1>Check-In</h1>
            <section className={styles.contentWrapper}>
                {
                    {
                        "hospital": vaga && medico && areaDeAtuacao &&
                        <>
                            <section className={styles.vaga}>
                                <section className={styles.vagaData}>
                                    <h1>Horário de início</h1>
                                    <div>{vaga.inicio.toLocaleTimeString("pt-BR", formatHora)}</div>
                                    <h1>Horário de término</h1>
                                    <div>{vaga.fim.toLocaleTimeString("pt-BR", formatHora)}</div>
                                    <h1>Data</h1>
                                    <div>{vaga.inicio.toLocaleDateString("pt-BR", formatData)}</div>
                                </section>
                                <section className={styles.vagaInfo}>
                                    <h1>Valor</h1>
                                    <div>{vaga.valor.toFixed(2)}</div>
                                    <h1>Área</h1>
                                    <div>{areaDeAtuacao.areaDeAtuacao}</div>
                                </section>
                            </section>

                            <section className={styles.candidato}>
                                <h1>Candidato</h1>

                                <div className={styles.candidatoInfo}>

                                    <h1>Nome</h1>
                                    <p>{medico.nome}</p>
                                    <h1>CRM</h1>
                                    <p>{formatCrmCnpj(medico.crm_cnpj)}/BR</p>
                                    <h1>Avaliação</h1>
                                    <p>{media ? media.toFixed(1) : "sem avaliações"}</p>

                                </div>
                            </section>

                            <section className={styles.codigoAutorizacao}>
                                <h1>Código de Autorização</h1>

                                <form onSubmit={handleFormSubmit} className={styles.codigoAutorizacaoForm}>
                                    <input type="text" value={codigoAutorizacao} onChange={handleAutorizacaoInputChange} />
                                    <button type="submit">Enviar</button>
                                </form>
                            </section>
                        </>,

                        "medico": <>
                            <Card
                                plantao={plantao}
                                usuario={usuario}
                                disabled={true}
                            />
                        </>
                    }[usuarioTipo]
                }
            </section>

        </div>

        { usuario.id == plantao.medicoId &&
            <div className={styles.modalCodigoAutorizacao}>
                <section className={styles.codigoAutorizacaoMedico}>
                    <h1>Código de Autorização</h1>
                    <p>{plantao.autorizacaoCheckIn}</p>
                </section>
                <button className={styles.continuarButton} onClick={() => navigate("../check-ins")}>Continuar</button>
            </div>
        }

    </div>
}