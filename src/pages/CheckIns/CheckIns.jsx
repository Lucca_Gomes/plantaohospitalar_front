import { useState, useEffect, useContext } from 'react';

import styles from "./check-ins.module.css";

import { Card } from "../../components/CheckIns/Card";
import { Paginacao } from "../../components/Paginacao/Paginacao";

import { ApplicationContext } from '../../contexts/ApplicationContextProvider';

export function CheckIns() {
    const { basePath, getUser } = useContext(ApplicationContext);

    const [ plantoes, setPlantoes ] = useState();
    const [ usuario, setUsuario ] = useState();

    const elementsPerPage = 5;
    const [ paginaIndex, setPaginaIndex ] = useState(1);
    const [ lastPaginaIndex, setLastPaginaIndex ] = useState(1);

    function loadPlantoes() {
        const url = `${basePath}/plantoes/usuario/${usuario.id}?disponivelPara=check-in`;
        const config = { 
            method: "GET", 
        };
        fetch(url, config) 
        .then(response => { response.json() 
            .then(data => {
                const updatedPlantoes = data;
                setPlantoes(updatedPlantoes);

                setPaginaIndex(1);

                const updatedLastPaginaIndex = Math.max(
                    Math.floor((updatedPlantoes.length - 1) / elementsPerPage) + 1,
                    1
                );
                setLastPaginaIndex(updatedLastPaginaIndex);
            });
        });
    }

    useEffect(() => {
        getUser().then((usuario) => setUsuario(usuario));
    }, []);

    useEffect(() => {
        if (!usuario) {
            return;
        }
        loadPlantoes()
    }, [usuario]);

    return <div className={styles.checkInsWrapper}>
        <h1>Check-In</h1>

        <section className={styles.cardsWrapper}>
            {(plantoes) &&
                Array.from({length: elementsPerPage}, (_, i) => (paginaIndex - 1) * elementsPerPage + i).map(
                    index => {
                        if (index >= plantoes.length) {
                            return;
                        }

                        const plantao = plantoes[index];

                        return <Card
                            key={index}
                            plantao={plantao}
                            usuario={usuario}
                            loadPlantoes={loadPlantoes}
                        />;
                    }
                )
            }

            <Paginacao
                paginaIndex={paginaIndex}
                lastPaginaIndex={lastPaginaIndex}
                setPaginaIndex={setPaginaIndex}
            />

        </section>
    </div>
}