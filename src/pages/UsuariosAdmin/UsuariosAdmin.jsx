import { useState, useEffect, useContext } from 'react';

import styles from './usuarios_admin.module.css';

import { ApplicationContext } from '../../contexts/ApplicationContextProvider';

import { AcoesButtons } from "../../components/UsuariosAdmin/AcoesButtons";

export function UsuariosAdmin() {
    const { basePath, token, secretKey } = useContext(ApplicationContext);
    const [ usuarios, setUsuarios ] = useState();

    function loadUsuarios() {
        const url = `${basePath}/usuarios?adminToken=${token}&adminSecretKey=${secretKey}` 
        const config = { 
            method: "GET", 
        }
        fetch(url, config) 
        .then(response => { response.json() 
            .then(data => {
                console.log("atualizando");
                setUsuarios(data);
            })
        })
    }

    useEffect(loadUsuarios, [])
    
    return <div className={styles.usuariosAdminWrapper}>
        <h1 className={styles.paginaTitulo}>Usuários</h1>

        <div className={styles.tabela}>
            <h1>#</h1>
            <h1>Nome</h1>
            <h1>User ID</h1>
            <h1>CRM/CNPJ</h1>
            <h1>Tipo</h1>
            <h1>Situação</h1>
            <h1>Ações</h1>

            { usuarios &&
                usuarios.map((usuario, index) => {
                    return [
                        <p key="index">{1 + index}</p>,
                        <p key="nome">{usuario.nome}</p>,
                        <p key="id">{usuario.id}</p>,
                        <p key="crm/cnpj">{usuario.crm_cnpj}</p>,
                        <p key="tipo">{usuario.tipo}</p>,
                        <p key="status">{usuario.status ? "Aceito" : "Pendente"}</p>,
                        <AcoesButtons
                            key="acoes"
                            usuarioId={usuario.id}
                            usuarioStatus={usuario.status}
                            loadUsuarios={loadUsuarios}
                        ></AcoesButtons>
                    ]
                })
            }
        </div>
    </div>
}
